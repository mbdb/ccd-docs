The easiest way to get started is to assign the ip address using the program 
IPset.exe

This program runs on Microsoft platforms. You should assign the IP address from an isolated network such as a PC with a crossover cable to the sensorProbe or cameraProbe in order to eliminate the risk of interfering with other computers on your network.

If you do not wish to use a Microsoft platform to assign the IP address please consult the html based help, for alternative IP assignment methods.

At the end of the Flash animation you can press the link to load
the Documentation, the IPset program, or the table of contents for this CD ROM.
If the flash doesn't load the help then you can load it manually with your
web browser.


The Documentation is located on the CD ROM at:
CDROM_DISK:\doc\index.html
Where CDROM_DISK may be G: for example


The Mib location:
CDROM_DISK:akcp_utilities\mib\sp.mib

The mib included in this package is called sp.mib. This is a complete
sensor/cameraProbe mib. This mib loads successfully under HP Openview,
Computer Associates Unicenter, UC Davis snmp package, Veritas.

We have also included rfc1213-MIB-II. This is a standard mib that is sometimes 
required to compile sp.mib