Description:



This directory contains snmp utilities to set and get and walk the mib.
The batch file cpInit.bat initializes the data fields of the cameraProbe8
using these snmp utilities.

If you have already set some of the fields in the cameraProbe8
cpInit.bat will overwrite those fields. You can also edit the file
cpInit.bat with your custom entries such as descriptions


Before running cpInit.bat, you should edit it.  On the line that says
"SET MIBDIRS=D:\ucd_utilities", change the drive letter "D:" to the CD-ROM
drive letter on your system.  For example, if you insert this CD into your
system on drive F:, after editting cpInit.bat, that line should look like
"SET MIBDIRS=F:\ucd_utilities"

The command syntax for sp2Init.bat is

"cpInit <ip_address> <community_string>"

where 	<ip_address> is the IP address of the cameraProbe8 unit
	<community_string> is the password for Administrator.  The default
	password is "public"
