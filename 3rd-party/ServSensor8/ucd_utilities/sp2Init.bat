::============================================================================
:: File name        : sp2Init.bat
:: last modified on : 16 February 2006
::============================================================================
::
:: Note:
:: =====
::
:: Before running this batch file, please change the DRIVE letter of 
:: the following SET command to the DRIVE letter of the CD-ROM on your system.
::
::============================================================================
::
SET MIBDIRS=D:\ucd_utilities
::
::
REM set the type of switch (sensorProbeSensorType) -> Water
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 4 || goto exit


REM setup the Water descriptions (sensorProbeSwitchDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.0  s "Water1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.1  s "Water2 Description" || goto exit


REM set the type of switch (sensorProbeSensorType) -> AC Voltage (voltage)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 12 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 12 || goto exit


REM setup the Voltage descriptions (sensorProbeSwitchDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.0  s "AC Voltage1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.1  s "AC Voltage2 Description" || goto exit


REM set the type of switch (sensorProbeSensorType) to Security
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 6 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 6 || goto exit


REM setup the Security descriptions (sensorProbeSwitchDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.0  s "Security1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.1  s "Security2 Description" || goto exit

REM set the type of switch (sensorProbeSensorType) to Siren
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 9 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 9 || goto exit

REM setup the Siren descriptions (sensorProbeSwitchDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.0  s "Siren1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.1  s "Siren2 Description" || goto exit

REM set the type of switch (sensorProbeSensorType) to Relay
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 13 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 13 || goto exit

REM setup the Relay descriptions (sensorProbeSwitchDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.0  s "Relay1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.1  s "Relay2 Description" || goto exit


REM set the type of switch (sensorProbeSensorType) to Motion Detector
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 14 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 14 || goto exit


REM setup the Motion Detector descriptions (sensorProbeSwitchDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.0  s "Motion Detector1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.1  s "Motion Detector2 Description" || goto exit


REM set the type of switch (sensorProbeSensorType) to Dry Contact
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 10 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 10 || goto exit


REM setup the Dry Contact switch descriptions (sensorProbeSwitchDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.0  s "Dry Contact Switch1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.1  s "Dry Contact Switch2 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.2  s "Dry Contact Switch3 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.3  s "Dry Contact Switch4 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.4  s "Dry Contact Switch5 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.5  s "Dry Contact Switch6 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.6  s "Dry Contact Switch7 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.7  s "Dry Contact Switch8 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.8  s "Dry Contact Switch9 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.9  s "Dry Contact Switch10 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.10  s "Dry Contact Switch11 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.1.11  s "Dry Contact Switch12 Description" || goto exit


REM Turn off all of the switches (sensorProbeSwitchGoOnline) -> offline(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.0 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.1 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.2 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.3 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.4 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.5 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.6 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.7 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.8 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.9 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.10 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.5.11 i 2 || goto exit

REM setup switches direction (sensorProbeSwitchDirection) -> input(0)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.6.0 i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.6.1 i 0 || goto exit


REM setup switches normal state (sensorProbeSwitchNormalState) -> open(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.0  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.1  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.2  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.3  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.4  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.5  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.6  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.7  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.8  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.9  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.10  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.7.11  i 1 || goto exit


REM setup switches output (sensorProbeSwitchOutputLevel) -> high(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.8.0 i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.8.1 i 1 || goto exit


REM setup the switch email trap limit (sensorProbeSwitchEmailTrapLimit) -> disable(0)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.1  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.2  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.3  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.4  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.5  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.6  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.7  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.8  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.9  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.10  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.11.11  i 0 || goto exit


REM setup the switch email trap schedule (sensorProbeSwitchEmailTrapSchedule) -> "0000000000000000000000000000"
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.0  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.1  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.2  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.3  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.4  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.5  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.6  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.7  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.8  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.9  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.10  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.12.11  s "0000000000000000000000000000" || goto exit


REM sensorProbeSwitchEmailTrapInterval -> 0 minutes
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.1  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.2  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.3  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.4  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.5  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.6  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.7  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.8  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.9  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.10  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.13.11  i 0 || goto exit


REM sensorProbeSwitchSendNormalTrap -> enable(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.0  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.1  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.2  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.3  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.4  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.5  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.6  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.7  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.8  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.9  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.10  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.14.11  i 1 || goto exit


REM sensorProbeSwitchDelayError -> 0 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.1  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.2  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.3  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.4  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.5  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.6  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.7  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.8  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.9  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.10  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.15.11  i 0 || goto exit


REM sensorProbeSwitchDelayNormal -> 0 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.1  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.2  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.3  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.4  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.5  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.6  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.7  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.8  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.9  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.10  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.16.11  i 0 || goto exit


REM sensorProbeSwitchSirenCycleTime -> 3 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.0  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.1  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.2  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.3  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.4  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.5  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.6  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.7  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.8  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.9  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.10  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.29.11  i 3 || goto exit


REM sensorProbeSwitchSirenOnPort -> N/A(255)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.0  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.1  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.2  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.3  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.4  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.5  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.6  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.7  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.8  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.9  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.10  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.30.11  i 255 || goto exit


REM sensorProbeSwitchSirenActiveStatus -> highCritical(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.1  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.2  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.3  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.4  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.5  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.6  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.7  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.8  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.9  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.10  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.31.11  i 4 || goto exit


REM sensorProbeSwitchSirenAction -> turn-off(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.1  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.2  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.3  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.4  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.5  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.6  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.7  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.8  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.9  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.10  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.32.11  i 4 || goto exit



REM sensorProbeSwitchRelayCycleTime -> 3 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.0  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.1  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.2  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.3  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.4  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.5  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.6  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.7  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.8  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.9  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.10  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.18.11  i 3 || goto exit


REM sensorProbeSwitchRelayOnPort -> N/A(255)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.0  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.1  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.2  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.3  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.4  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.5  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.6  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.7  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.8  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.9  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.10  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.19.11  i 255 || goto exit


REM sensorProbeSwitchRelayActiveStatus -> highCritical(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.1  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.2  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.3  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.4  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.5  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.6  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.7  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.8  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.9  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.10  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.20.11  i 4 || goto exit


REM sensorProbeSwitchRelayAction -> turn-off(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.1  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.2  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.3  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.4  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.5  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.6  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.7  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.8  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.9  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.10  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.21.11  i 4 || goto exit


REM setup Manual Relay cycle time for Relay sensor (sensorProbeSwitchManualRelayCycleTime) -> 3 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.24.0 i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.24.1 i 3 || goto exit


REM setup Manual Relay Action for Relay sensor (sensorProbeSwitchManualRelayAction) -> allow-sensor-control(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.25.0 i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.25.1 i 1 || goto exit


REM setup Description of Relay On (sensorProbeSwitchRelayDescOn)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.26.0 s "" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.26.1 s "" || goto exit


REM setup Description of Relay Off (sensorProbeSwitchRelayDescOff)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.27.0 s "" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.27.1 s "" || goto exit


REM sensorProbeTempDescription
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.1.0 s "Temperature1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.1.1 s "Temperature2 Description" || goto exit


REM sensorProbeTempGoOnline -> goOffline(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.6.0 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.6.1 i 2 || goto exit


REM setup the temperature sensor thresholds

REM sensorProbeTempDegreeType -> celcius(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.12.0 i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.12.1 i 1 || goto exit


REM sensorProbeTempHighWarning -> 40
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.7.0 i 40 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.7.1 i 40 || goto exit


REM sensorProbeTempHighCritical -> 55
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.8.0 i 55 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.8.1 i 55 || goto exit


REM sensorProbeTempLowWarning -> 15
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.9.0 i 15 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.9.1 i 15 || goto exit


REM sensorProbeTempLowCritical -> 5
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.10.0 i 5 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.10.1 i 5 || goto exit


REM sensorProbeTempRearm -> 1
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.11.0 i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.11.1 i 1 || goto exit

REM sensorProbeTempDegreeType -> fahr(0)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.12.0 i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.12.1 i 0 || goto exit

REM sensorProbeTempHighWarning -> 80
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.7.0 i 80 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.7.1 i 80 || goto exit


REM sensorProbeTempHighCritical -> 85
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.8.0 i 85 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.8.1 i 85 || goto exit


REM sensorProbeTempLowWarning -> 60
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.9.0 i 60 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.9.1 i 60 || goto exit


REM sensorProbeTempLowCritical -> 55
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.10.0 i 55 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.10.1 i 55 || goto exit


REM sensorProbeTempRearm -> 2
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.11.0 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.11.1 i 2 || goto exit


REM sensorProbeTempEmailTrapLimit -> disable(0)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.16.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.16.1  i 0 || goto exit

REM sensorProbeTempEmailTrapSchedule -> "0000000000000000000000000000"
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.17.0  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.17.1  s "0000000000000000000000000000" || goto exit


REM sensorProbeTempEmailTrapInterval -> 0 minutes
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.18.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.18.1  i 0 || goto exit


REM sensorProbeTempSendNormalTrap -> enable(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.19.0  i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.19.1  i 1 || goto exit


REM sensorProbeTempDelayError -> 0 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.20.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.20.1  i 0 || goto exit


REM sensorProbeTempDelayNormal -> 0 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.21.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.21.1  i 0 || goto exit


REM sensorProbeTempSirenCycleTime -> 3 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.30.0  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.30.1  i 3 || goto exit


REM sensorProbeTempSirenOnPort -> N/A(255)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.31.0  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.31.1  i 255 || goto exit


REM sensorProbeTempSirenActiveStatus -> highCritical(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.32.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.32.1  i 4 || goto exit


REM sensorProbeTempSirenAction -> turn-off(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.33.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.33.1  i 4 || goto exit


REM sensorProbeTempRelayCycleTime -> 3 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.23.0  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.23.1  i 3 || goto exit


REM sensorProbeTempRelayOnPort -> N/A(255)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.24.0  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.24.1  i 255 || goto exit


REM sensorProbeTempRelayActiveStatus -> highCritical(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.25.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.25.1  i 4 || goto exit


REM sensorProbeTempRelayAction -> turn-off(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.26.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.26.1  i 4 || goto exit

REM sensorProbeTempOffset -> 0
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.29.0  i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.16.1.29.1  i 0 || goto exit

REM set the type of switch (sensorProbeSensorType) to Airflow
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 8 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 8 || goto exit


REM setup the Airflow descriptions (sensorProbeHumidityDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.1.0  s "Airflow1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.1.1  s "Airflow2 Description" || goto exit


REM set the sensor type (sensorProbeSensorType) to 4 to 20 mA sensor 
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 2 || goto exit


REM setup the 4-20mAmp descriptions (sensorProbeHumidityDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.1.0 s "4-20mAmp1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.1.1 s "4-20mAmp2 Description" || goto exit


REM set the sensor type (sensorProbeSensorType) to DC Voltage sensor (atod)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 5 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 5 || goto exit


REM setup the DC Voltage descriptions (sensorProbeHumidityDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.1.0 s "DC Voltage1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.1.1 s "DC Voltage2 Description" || goto exit


REM set the sensor type (sensorProbeSensorType) to Humidity 
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 3 || goto exit


REM setup the humidity descriptions (sensorProbeHumidityDescription)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.1.0 s "Humidity1 Description" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.1.1 s "Humidity2 Description" || goto exit


REM sensorProbeHumidityGoOnline -> goOffline(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.6.0 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.6.1 i 2 || goto exit


REM setup the humidity sensor thresholds


REM sensorProbeHumidityHighWarning -> 60
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.7.0 i 60 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.7.1 i 60 || goto exit


REM sensorProbeHumidityHighCritical -> 90
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.8.0 i 80 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.8.1 i 80 || goto exit


REM sensorProbeHumidityLowWarning -> 15
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.9.0 i 15 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.9.1 i 15 || goto exit


REM sensorProbeHumidityLowCritical -> 10
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.10.0 i 10 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.10.1 i 10 || goto exit


REM sensorProbeHumidityRearm -> 5
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.11.0 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.11.1 i 2 || goto exit


REM sensorProbeHumidityLowVoltage -> 4(40)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.14.0 i 40 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.14.1 i 40 || goto exit


REM sensorProbeHumidityHighVoltage -> 20(200)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.15.0 i 200 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.15.1 i 200 || goto exit


REM sensorProbeHumidityEmailTrapLimit -> disable(0)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.17.0 i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.17.1 i 0 || goto exit


REM sensorProbeHumidityEmailTrapSchedule -> "0000000000000000000000000000"
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.18.0  s "0000000000000000000000000000" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.18.1  s "0000000000000000000000000000" || goto exit


REM sensorProbeHumidityEmailTrapInterval -> 0 minutes
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.19.0 i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.19.1 i 0 || goto exit


REM sensorProbeHumiditySendNormalTrap -> enable(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.20.0 i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.20.1 i 1 || goto exit


REM sensorProbeHumidityDelayError -> 0 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.21.0 i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.21.1 i 0 || goto exit


REM sensorProbeHumidityDelayNormal -> 0 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.22.0 i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.22.1 i 0 || goto exit


REM sensorProbeHumidityAtoDAmountMaxVoltage -> 20(200)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.24.0 i 200 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.24.1 i 200 || goto exit


REM sensorProbeHumidityAtoDAmountBaseVoltage -> 0
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.25.0 i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.25.1 i 0 || goto exit


REM sensorProbeHumidityAtoDTypeUnit -> absolute(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.26.0 i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.26.1 i 1 || goto exit


REM sensorProbeHumidityDcUnit -> "Volts"
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.27.0 s "Volts" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.27.1 s "Volts" || goto exit


REM sensorProbeHumidityAtoDJumper -> 20(200) Volts
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.28.0 i 200 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.28.1 i 200 || goto exit


REM sensorProbeHumiditySirenCycleTime -> 3 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.37.0  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.37.1  i 3 || goto exit


REM sensorProbeHumiditySirenOnPort -> N/A(255)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.38.0  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.38.1  i 255 || goto exit


REM sensorProbeHumiditySirenActiveStatus -> highCritical(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.39.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.39.1  i 4 || goto exit


REM sensorProbeHumiditySirenAction -> turn-off(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.40.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.40.1  i 4 || goto exit

REM sensorProbeHumidityRelayCycleTime -> 3 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.29.0  i 3 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.29.1  i 3 || goto exit


REM sensorProbeHumidityRelayOnPort -> N/A(255)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.30.0  i 255 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.30.1  i 255 || goto exit


REM sensorProbeHumidityRelayActiveStatus -> highCritical(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.31.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.31.1  i 4 || goto exit


REM sensorProbeHumidityRelayAction -> turn-off(4)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.32.0  i 4 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.32.1  i 4 || goto exit

REM sensorProbeHumidity4to20mAUnit -> "mAmp"
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.34.0 s "mAmp" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.34.1 s "mAmp" || goto exit

REM sensorProbeHumidityOffset -> 0
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.36.0 i 0 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.17.1.36.1 i 0 || goto exit

REM set the type of sensor to no detect (sensorProbeSensorType)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.0 i 15 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.18.1.9.1 i 15 || goto exit


REM mib II variables
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.2.1.1.4.0 s "Sys Contact" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.2.1.1.5.0 s "Sys Name" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.2.1.1.6.0 s "Sys Location" || goto exit


REM spHelpUrl
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.1.7.0 s "Ravica.com/help/sp/index.html" || goto exit


REM sensorProbeUseDHCP -> no(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.2.0 i 2 || goto exit


REM TRAPS and DEBUG
REM sensorProbeDebug off(0)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.20.0 i 0 || goto exit


REM sensorProbeTrapResend -> on(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.22.0 i 1 || goto exit


REM sensorProbeTrapResendInterval -> 60 seconds
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.23.0 i 60 || goto exit


REM sensorProbeSendTraps -> off(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.24.0 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.24.1 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.24.2 i 2 || goto exit


REM sensorProbeTrapDestination 255.255.255.255
REM spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.25.0 a 192.168.0.1 || goto exit
REM spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.25.1 a 192.168.0.1 || goto exit
REM spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.25.2 a 192.168.0.1 || goto exit

REM sensorProbeTrapCommunity
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.26.0 s "public" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.26.1 s "public" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.26.2 s "public" || goto exit


REM sensorProbeSendMail off(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.30.0 i 2 || goto exit

REM sensorProbeMailRecpt
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.31.0 s "" || goto exit

REM sensorProbeMailFrom
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.32.0 s "" || goto exit

REM sensorProbeMailSMTP ""
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.33.0 s "" || goto exit

REM sensorProbeMailJpgInline -> link(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.34.0 i 2 || goto exit

REM sensorProbeMailResendInterval
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.36.0 i 60 || goto exit

REM sensorProbeMailMaxResend -> send once(0)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.37.0 i 0 || goto exit


REM sensorProbeTrapMailPollInterval
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.50.0 i 1 || goto exit

REM sensorProbeDataCollectionPeriod
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.53.0 i 15 || goto exit

REM sensorProbeMailTimeout
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.54.0 i 15 || goto exit

REM sensorProbeAutoSense -> enable(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.55.0 i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.55.1 i 1 || goto exit

REM sensorProbeUsePassword -> doNotUsePassword(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.57.0 i 1 || goto exit

::REM sensorProbeDisplayLogo -> disable(0)
::spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.59.0 i 0 || goto exit

REM sensorProbeTrapType -> specificTypeTrap(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.60.0 i 1 || goto exit

REM sensorProbeMailCC -> ""
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.61.0 s ""  || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.61.1 s "" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.61.2 s "" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.61.3 s "" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.61.4 s "" || goto exit

REM sensorProbeAllowIPChange -> enable(1)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.62.0 i 1 || goto exit

REM sensorProbeEnableSysLog -> enableLogToFlash
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.66.0 i 1 || goto exit

REM sensorProbeClearSysLog -> allMessages(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.68.0 i 2 || goto exit

REM sensorProbeSyslogDestIP -> 192.168.0.1
REM spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.69.0 a 192.168.0.1 || goto exit

REM sensorProbeSyslogPort -> 514
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.70.0 u 514 || goto exit

:: sensorProbeTimeZone -> 0
::spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.83.0 i 0 || goto exit

REM sensorProbeNtpMode -> notUse(0)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.84.0 i 0 || goto exit

REM sensorProbeNtpServer -> ""
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.85.0 s "" || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.85.1 s "" || goto exit

REM sensorProbeSMTPAuth -> disable(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.87.0 i 2 || goto exit

REM sensorProbeDNSServer
REM spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.90.0 a 192.168.0.1 || goto exit

REM sensorProbeAltWebPort -> 8080
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.91.0 u 8080 || goto exit

REM sensorProbeSendTrapsAlive -> off(2)
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.93.0 i 2 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.93.1 i 2 || goto exit

REM sensorProbeTrapReIntervalAlive -> 1 mins
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.94.0 i 1 || goto exit
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.94.1 i 1 || goto exit

REM sensorProbeSubnetMask 255.255.255.0
spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.28.0 a 255.255.255.0 || goto exit

REM sensorProbeDefaultGateway 192.168.0.1
REM spsnmpset -m all -v1 -t 12 -c %1 %2 .1.3.6.1.4.1.3854.1.2.2.1.27.0 a 192.168.0.1 || goto exit

:exit
