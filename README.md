## Description:

Overall technical documentation around Concordia Seismological station

## Getting started:

Open the index file: CCD-DOCS-001-index

## Converting files to pdf

Run the 2pdf.run script:

```
./2pdf.run

```